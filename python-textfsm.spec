%global _empty_manifest_terminate_build 0
Name:           python-textfsm
Version:        1.1.3
Release:        1
Summary:        Python module for parsing semi-structured text into python tables.
License:        Apache-2.0
URL:            https://github.com/google/textfsm
Source0:        https://files.pythonhosted.org/packages/b8/bf/c9147d29c5a3ff4c1c876e16ea02f6d4e4f35ba1bcbb2ac80a254924f0aa/textfsm-1.1.3.tar.gz
BuildArch:      noarch
%description
Python module which implements a template based state machine for parsing
semi-formatted text. Originally developed to allow programmatic access to
information returned from the command line interface (CLI) of networking
devices.

%package -n python3-textfsm
Summary:        Python module for parsing semi-structured text into python tables.
Provides:       python-textfsm
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-six
Requires:       python3-future
Requires:       python3-six
Requires:       python3-pytest-runner
%description -n python3-textfsm
Python module which implements a template based state machine for parsing
semi-formatted text. Originally developed to allow programmatic access to
information returned from the command line interface (CLI) of networking
devices.

%package help
Summary:        Python module for parsing semi-structured text into python tables.
Provides:       python3-textfsm-doc
%description help
Python module which implements a template based state machine for parsing
semi-formatted text. Originally developed to allow programmatic access to
information returned from the command line interface (CLI) of networking
devices.

%prep
%autosetup -n textfsm-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-textfsm -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu  Dec 08 2022 wubijie <wubijie@kylinos.cn> - 1.1.3-1
- Update package to version 1.1.3

* Wed May 18 2022 OpenStack_SIG <openstack@openeuler.org> - 1.1.2-1
- Upgrade package python3-textfsm to version 1.1.2

* Thu Mar 3 2022 zhaoshuang <zhaoshuang@uniontech.com>
- remove some unnecessary buildrequires

* Fri Aug 6 2021 huangtianhua <huangtianhua@huawei.com>
- Adds pip/pbr/pytest-runner as BuildRequires

* Mon Mar 15 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
